output "instances-ip" {
  value = [for instance in aws_instance.server : instance.public_ip]
}
