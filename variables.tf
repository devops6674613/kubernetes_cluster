variable "path_to_keys" {
  type        = list(string)
  description = ""
}
variable "aws_region" {
  type        = string
  description = ""
}
variable "aws_profile" {
  type        = string
  description = ""
}
variable "vpc_cidr_block" {
  type        = string
  description = ""
}
variable "env_prefix" {
  type        = string
  description = ""
}
variable "subnet_cidr_block" {
  type        = string
  description = ""
}
variable "my_ip" {
  type        = string
  description = ""
}
variable "public_key_location" {
  type        = string
  description = ""
}
variable "ec2_instance_settings" {
  description = ""
  type = map(object({
    instance_type = string
    tag           = string
  }))
}
