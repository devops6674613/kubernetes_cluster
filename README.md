# Kubernetes Cluster configuration
## Purpose
The aim of the project is to learn IaC using Terraform and Ansible tools.
Additionally, the project will enable the rapid creation of infrastructure that will then be used to learn
and test various types of solutions based on Kubernetes.
The terraform code creates two servers in Amazon Web Services (AWS), a Master node and a Worker node.
However, thanks to IaC, we can easily and quickly increase the number of Worker nodes depending on our needs.
##  How to use it
- An AWS account is required, as are SSH keys.
- clone the repository
- fill in the variables in the `terraform.tfvars` file with your values
- initialize your working directory by running the `terraform init` command
- preview th execution plan using the `terraform plan` command
- apply plan using the `terraform apply` command
- all should work :wink:
### Ansible
Once the entire infrastructure on AWS is created, install the kubeadm tool and then use it to create a cluster.
We will use Ansible for this purpose.
- I have used `aws_ec2` plugin to get inventory hosts from Amazon Web Services EC2
- create `ansible.cfg` configuration file with content:
 ```
[defaults]
host_key_checking = <your_value>
inventory = <path_to_inventory_file>

interpreter_python = <path_to_python3_file>
enable_plugins = <plugin_name>

private_key_file = <path_to_ssh_key>
remote_user = <username>
```
- Write `aws_ec2` plugin configuration:
```
plugin: aws_ec2
aws_profile: <profile_name>

regions:
    - <region>

filters:
    instance-state-name: <value>

hostnames:
    - name: '<value>'

keyed_groups:
    - key: tags
    prefix: tag
```
To run your playbook, use the ansible command: `ansible-playbook playbook.yaml`
Thank you for reading :wink:

---
**If you would like to contact me, here are my contact details:**<br>
:link: Check out my [LinkedIn profile](https://www.linkedin.com/in/dariusz-klimowicz/)<br>
:mailbox: E-mail: <dariusz.secop@gmail.com>
