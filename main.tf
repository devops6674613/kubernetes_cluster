provider "aws" {
  region                   = var.aws_region
  profile                  = var.aws_profile
  shared_credentials_files = var.path_to_keys
}

resource "aws_vpc" "app-vpc" {
  cidr_block           = var.vpc_cidr_block
  enable_dns_hostnames = true

  tags = {
    Name : "${var.env_prefix}-vpc"
  }
}

resource "aws_subnet" "app-subnet" {
  vpc_id     = aws_vpc.app-vpc.id
  cidr_block = var.subnet_cidr_block

  tags = {
    Name : "${var.env_prefix}-subnet"
  }
}

resource "aws_internet_gateway" "app-igw" {
  vpc_id = aws_vpc.app-vpc.id

  tags = {
    Name : "${var.env_prefix}-igw"
  }
}

resource "aws_route_table" "app-route-table" {
  vpc_id = aws_vpc.app-vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.app-igw.id
  }

  tags = {
    Name : "${var.env_prefix}-rtb"
  }
}

resource "aws_route_table_association" "assoc-rtb-subnet" {
  subnet_id      = aws_subnet.app-subnet.id
  route_table_id = aws_route_table.app-route-table.id
}

resource "aws_security_group" "master-sg" {
  name   = "app-master-sg"
  vpc_id = aws_vpc.app-vpc.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "TCP"
    cidr_blocks = [var.my_ip]
  }

  ingress {
    from_port   = 6443
    to_port     = 6443
    protocol    = "TCP"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 2379
    to_port     = 2380
    protocol    = "TCP"
    cidr_blocks = [var.vpc_cidr_block]
  }

  ingress {
    from_port   = 10250
    to_port     = 10252
    protocol    = "TCP"
    cidr_blocks = [var.vpc_cidr_block]
  }

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "TCP"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
    prefix_list_ids = []
  }

  tags = {
    Name : "${var.env_prefix}-master-sg"
  }
}

resource "aws_security_group" "worker-sg" {
  name   = "app-worker-sg"
  vpc_id = aws_vpc.app-vpc.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "TCP"
    cidr_blocks = [var.my_ip]
  }

  ingress {
    from_port   = 10250
    to_port     = 10250
    protocol    = "TCP"
    cidr_blocks = [var.vpc_cidr_block]
  }

  ingress {
    from_port   = 30000
    to_port     = 32767
    protocol    = "TCP"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "TCP"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
    prefix_list_ids = []
  }

  tags = {
    Name : "${var.env_prefix}-worker-sg"
  }
}

data "aws_ami" "latest-ubuntu-image" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-jammy-*-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }
}

resource "aws_key_pair" "ssh-key" {
  key_name   = "server-key"
  public_key = file(var.public_key_location)
}

resource "aws_instance" "server" {
  for_each      = var.ec2_instance_settings
  ami           = data.aws_ami.latest-ubuntu-image.id
  instance_type = each.value.instance_type
  subnet_id     = aws_subnet.app-subnet.id

  associate_public_ip_address = true
  key_name                    = aws_key_pair.ssh-key.key_name

  tags = {
    Name : "${var.env_prefix}-${each.value.tag}"
  }
}

resource "aws_network_interface_sg_attachment" "master-sg-attachment" {
  security_group_id    = aws_security_group.master-sg.id
  network_interface_id = aws_instance.server["master-node"].primary_network_interface_id
}

resource "aws_network_interface_sg_attachment" "worker-sg-attachment" {
  security_group_id    = aws_security_group.worker-sg.id
  network_interface_id = aws_instance.server["worker-node"].primary_network_interface_id
}
